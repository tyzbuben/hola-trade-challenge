## Hola Trade Challenge

Link for article where you can find the rules and conditions of the competition -  https://habr.com/company/hola/blog/414723/

The code templates and all environment files were provided by Hole Team! Thanks them! 
Things to check out :

1. Ebobot.js - my trading algorithm(final version)  - checkout ebobot branch!
2. logparser***.jar - primitive log parser to check out my trade bot's statistics.
3. launchLog.sh - litle script to  make my life easier while launching bot on testing arena
4. Enormous amount of prevous version of trading algo.

---
P.S. JavaScript is not my language, just used it for contest.