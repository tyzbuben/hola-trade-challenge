'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log) {
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.THERESHOLD = 7;
        this.minimal = -1;
        this.min_index;
        this.return_values = this.values.slice();
        this.return_counts = this.counts.slice();
        this.outerArr = [];
        for (let i = 0; i < counts.length; i++)
            this.total += counts[i] * values[i];
    }
    summarize(arr, vals) {
        let sum = 0;
        for (let i = 0; i < arr.length; i++)
            sum += vals[i] * arr[i];
        return sum;
    }

    minimize() {
        if (this.minimal < 0) {
            this.minimal = 0;
            this.log(`minimal = -1`)
            for (let i = 0; i < this.return_counts.length; i++) {
                if (!this.values[i])
                    this.return_counts[i] = 0;
            }
            this.min_index = this.next_min();
            return this.return_counts;
        }
        while (true) {
            if (this.return_counts[this.min_index] == 0) {
                this.log(`enternity`)
                this.log(`${this.return_counts}`)
                this.min_index = this.next_min();
            } else {
                this.return_counts[this.min_index]--;
                return this.return_counts;
            }
        }
    }

    next_min() {
        let min;
        let minimal_index;
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0) {
                min = this.return_values[i];
                break;
            }
        }
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0
                && this.return_values[i] <= min
                && this.return_values[i] >= this.minimal) {
                min = this.return_values[i];
                this.minimal = min;
                minimal_index = i;
                this.return_values[i] = 0;
                this.log(`находим минимальный`)
            }
        }
        return minimal_index;
    }

   

    addToOuter(arr){
        if(this.outerArr.length==0){
            this.outerArr.push(arr);
            return;
        }
        let hasDuplicate = true;
        for(let i = 0;i<this.outerArr.length;i++){
                hasDuplicate = this.equal(this.outerArr[i],arr);
        }
        if(!hasDuplicate){
            this.outerArr.push(arr);
        }
    }

    offer(o) {
        this.log(`${this.rounds} rounds left - simple_str_bot`);
        this.rounds--;
        let sum;
        if (o) {
            sum = this.summarize(o, this.values);
            if (this.rounds == 0) {
                return;
            }
            if (sum >= this.THERESHOLD)
                return;
            //if (sum >= this.total / 2)
            // return;
        }
        o = this.counts.slice();
        this.outerArr.push(o);
        this.minimi(o, 0);
        //this.positiveOuter(this.outerArr);
        this.log(`${this.outerArr} OUTER ARRAY`);
        //this.clearArr(this.outerArr);
        this.log(JSON.stringify(this.outerArr, null, 4) + `CLEAR OUTER ARRAY`);
        this.log(`${this.counts} counts`);
        //return this.minimize();
        return;
    }

    minimi(o, index) {
        let arr = o.slice();
        this.log(`${arr} - arr,${index} - index dive into`);
        if (index == arr.length) {
            this.log(` return`);
            return;
        }

        for (let i = arr[index]; i >= 0; i--) {
            arr[index] = i;
            this.log(`${arr[index]} index value`);
            this.log(`${arr} inner ARRAY`);
            let additionArr = arr.slice();
            this.outerArr.push(additionArr);
            this.addToOuter(additionArr);
            //arr[index] = i - 1;
            //this.minimi(arr, index + 1);
            arr[index] = i;
            this.minimi(arr, index +1);
            this.minimi(arr, index +2);
        }
    }

    positiveOuter(arr) {
        for (let i = 0; i < arr.length; i++) {
            this.makeValuesPositive(arr[i]);
        }
    }

    makeValuesPositive(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                arr[i] = arr[i] * (-1);
            }
        }
    }

    equal(arr1, arr2) {
        let equal = true;
        for (let i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                equal = false;
            }
        }
        return equal;
    }
};



/* clearArr(arr){
let a =[];
for(let i =0;i<arr.length;i++){
if(!this.equalInArr(a,arr[i])){
a.push(arr[i]);
}
}
}*/

/*equalInArr(arr,elem){
if(!elem){return false;}
let equal = false;
for(let i = 0;i<arr.length;i++){
equal = this.equal(arr[i],elem);
}
return equal;
}*/

/* equal(arr1,arr2){
let equal = true;
for(let i =0;i<arr1.length;i++){
if(arr1[i]!=arr2[i]){
equal = false;
}
}
return equal;
}*/