'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log) {
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.minimal = -1;
        this.min_index;
        this.return_values = this.values.slice();
        this.return_counts = this.counts.slice();
        for (let i = 0; i < counts.length; i++)
            this.total += counts[i] * values[i];
    }
    summarize(arr, vals) {
        let sum = 0;
        for (let i = 0; i < arr.length; i++)
            sum += vals[i] * arr[i];
        return sum;
    }


    minimize() {
        if (this.minimal < 0 || this.rounds>=3) {
            this.minimal = 0;
            for (let i = 0; i < this.return_counts.length; i++) {
                if (!this.values[i])
                this.return_counts[i] = 0;
            }
            this.min_index = this.next_min();
            return this.return_counts;
        }

        if(this.rounds>=3){
            return this.return_counts;
        }
        while (true) {
            if (this.return_counts[this.min_index] == 0) {
                this.min_index = this.next_min();
            } else {
                this.return_counts[this.min_index]=this.return_counts[this.min_index]--;
                return this.return_counts;
            }
        }
    }

    next_min() {
        let min;
        let minimal_index;
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0) {
                min = this.return_values[i];
                break;
            }
        }
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0
                && this.return_values[i] <= min
                && this.return_values[i] >= this.minimal) {
                min = this.return_values[i];
                this.minimal = min;
                minimal_index = i;
                this.return_values[i]=0;
            }
        }
        return minimal_index;
    }
    offer(o) {
        this.rounds--;
        let sum;
        if (o) {
            sum = this.summarize(o, this.values);
            //if(sum>=this.THERESHOLD)
            //    return;
            if (sum >= this.total / 2)
                return;
            if(this.rounds<=0)
                return;
        }
        return this.minimize();
    }
};
