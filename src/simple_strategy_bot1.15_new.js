'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log) {
        this.me = me;
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.THERESHOLD = 7;
        this.minimal = -1;
        this.min_index;
        this.return_values = this.values.slice();
        this.return_counts = this.counts.slice();
        this.outerArr = [];
        for (let i = 0; i < counts.length; i++) {
            this.total += counts[i] * values[i];
        }
        this.offerIndex;
        this.opponentOffer;
    }
    summarize(arr) {
        let sum = 0;
        for (let i = 0; i < arr.length; i++)
            sum += this.values[i] * arr[i];
        return sum;
    }

    addToOuter(arr) {
        if (this.outerArr.length == 0) {
            this.outerArr.push(arr);
            return;
        }
        let hasDuplicate = true;
        for (let i = 0; i < this.outerArr.length; i++) {
            let a = this.equal(this.outerArr[i], arr);
            if (a) {
                return;
            }
            hasDuplicate = a;

        }
        if (!hasDuplicate) {
            this.outerArr.push(arr);
        }
    }

    saveOppnentOffer(o) {
        if (!this.opponentOffer) {
            this.opponentOffer = o;
            return;
        }
        if (this.summarize(this.opponentOffer) < this.summarize(o)) {
            this.opponentOffer = o.slice();
        }
    }

    offer(o) {
        //this.log(`ROUND`);
        //this.log(`${this.me}`);
        if (this.rounds == 5) {
            this.minimi(this.counts, 0);
            this.deleteUneven();
            this.quickSort(this.outerArr, 0, this.outerArr.length - 1);
            //this.log(`COUNTED ONCE`);
        }
        //this.log(`ROUND ${this.rounds}`);
        //this.log(`${this.rounds} rounds left - simple_str_bot`);
        if (o) {
            //this.log(`OFFER`);
            //this.log(`ENTERED INSIDE`);
            let sum;
            sum = this.summarize(o, this.values);
            this.saveOppnentOffer(o);
            if (this.rounds == 1 && sum != 0 && sum > this.opponentOffer) {
                //this.log(`LAST CHANCEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA`);
                return;
            }
            if (sum >= this.total / 2)
                return;
            //if (sum >= this.total / 2){
            //this.log(`${sum} SUM IS GOOD ENOUGH`);
            // return;}
        }
        //o = this.counts.slice();
        //this.outerArr.push(o);
        //this.minimi(o, 0);
        //this.positiveOuter(this.outerArr);
        //this.log(`${this.outerArr} OUTER ARRAY`);
        //this.clearArr(this.outerArr);
        //this.log(JSON.stringify(this.outerArr, null, 4) + `CLEAR OUTER ARRAY`);
        //this.log(`${this.counts} counts`);
        //this.quickSort(this.outerArr, 0, this.outerArr.length - 1);
        //for (let i = 0; i < this.outerArr.length; i++) {
        // this.log(`VALUER - ${this.summarize(this.outerArr[i])}`);
        //}
        //return this.minimize();
        //this.log(`GOING TO MAKE OFFER`);
        return this.makeOffer();
    }

    deleteUneven() {
        for (let i = 0; i < this.outerArr.length; i++) {
            if (this.outerArr[i][0] == this.counts[0] &&
                this.outerArr[i][1] == this.counts[1]
                && this.outerArr[i][2] == this.counts[2]) {
                this.outerArr.splice(i, 1);
                break;
            }
        }
    }

    makeOffer() {
        this.log(`CAME INSIDE MAKEOFFER`);
        this.rounds--;

        if (!this.offerIndex) {
            this.offerIndex = this.outerArr.length - 2;
            let offer = this.outerArr[this.offerIndex];
            this.offerIndex--;
            this.log(`${offer}`);
            return offer;
        }

        if (this.rounds == 1 && this.summarize(this.opponentOffer) > 0) {
            return this.opponentOffer;
        }
        let offer = this.outerArr[this.offerIndex];
        this.offerIndex--;
        this.log(`${offer}`);
        return offer;
    }

    minimi(o, index) {
        let arr = o.slice();
        //this.log(`${arr} - arr,${index} - index dive into`);
        if (index == arr.length) {
            return;
        }

        for (let i = arr[index]; i >= 0; i--) {
            arr[index] = i;
            //this.log(`${arr[index]} index value`);
            //this.log(`${arr} inner ARRAY`);
            let additionArr = arr.slice();
            //this.outerArr.push(additionArr);
            this.addToOuter(additionArr);
            //arr[index] = i - 1;
            //this.minimi(arr, index + 1);
            arr[index] = i;
            this.minimi(arr, index + 1);
            this.minimi(arr, index + 2);
        }
    }

    equal(arr1, arr2) {
        let equal = true;
        for (let i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                equal = false;
            }
        }
        return equal;
    }

    swap(arr, leftVal, rightVal) {
        let temp = arr[leftVal];
        arr[leftVal] = arr[rightVal];
        arr[rightVal] = temp;
    }

    partition(arr, left, right) {
        let pivot = Math.floor((right + left) / 2);
        let i = left;
        let j = right;
        while (i <= j) {
            while (this.summarize(arr[i]) < this.summarize(arr[pivot])) {
                i++;
            }
            while (this.summarize(arr[j]) > this.summarize(arr[pivot])) {
                j--;
            }
            if (i <= j) {
                this.swap(arr, i, j);
                i++; j--;
            }
        }
        return i;
    }

    quickSort(arr, left, right) {
        let index;
        if (arr.length > 1) {
            index = this.partition(arr, left, right);

            if (left < index - 1) {
                this.quickSort(arr, left, index - 1);
            }

            if (index < right) {
                this.quickSort(arr, index, right);
            }
        }
        return arr;
    }
};