'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log){
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.THERESHOLD = 7;
        this.minimal = -1;
        for (let i = 0; i<counts.length; i++)
            this.total += counts[i]*values[i];
    }   
    summarize(arr,vals){
        let summa = 0;
        for (let i = 0; i<arr.length; i++)
            summa += vals[i]*arr[i];
        return summa;
    }

    next_minimum(){
        let min = this.values[0];
        let summer;
        for(let i=0;i<this.values.length-1;i++){
            for(let j = 1;j<this.values.length;j++){
                summer = this.values[i]+this.values[j];
                if(summer!=0&&summer<min&&summer>this.minimal){
                    min = summer;   
                }
            }
        }
        return min;
    }

    minimize(){
        let a;
        let b = this.counts.slice();
        let c;
        for(let i = 0;i<this.values.length;i++){
            if(!this.values[i]){
                b[i] = 0;
            }
        }
        if(this.minimal<0){
            this.minimal = 0;
            this.log(`minimal = -1`)
            return b;
        }

        while(true){
            this.minimal=this.next_minimum();
            this.log(`${this.minimal} = current minimal`)
            c = b.slice();
            a = this.rec(c,0);
            this.log(`${a} - values of minimal amount`)
            if(a||this.minimal>10){
                this.log(`minimum was found ${this.minimal}`)
                break;
            }
        }
        return this.arr_differ(this.counts,a); 
    }

    rec(arr,start){
        this.log(`entered recursion`)
        let a = false;
        let rec_arr = arr.slice();
        for(let i = 0;i<rec_arr.length;i++){
            if(rec_arr[i]!=0){
                a = true;
            }
        }
        if(!a){
            this.log(`EMPTY ARR IN RECURSION`)
            return;
        }
        if(this.summarize(rec_arr,this.values)==this.minimal){
            this.log(`!@#$% GOT MINIMUM!!!`)
            return rec_arr;
        }
        let counter = start;
        while(true){
            if(counter==rec_arr.length){
                return;
            }
            if(rec_arr[counter]!=0){
                rec_arr = rec_arr.slice();
                let value = rec_arr[counter];
                value--;
                rec_arr[counter] = value;
                let c = counter+1;
                let end = this.rec(rec_arr,c);
                if(end){
                    return rec_arr;
                }
            } else {
                    counter++;
            }
        }
    }

    arr_differ(arr,arr_dif){
        for(let i=0;i<arr.length;i++){
            arr[i]-arr_dif[i];
        }
        return arr;
    }

    offer(o){
        this.log(`${this.rounds} rounds left - simple_str_bot`);
        this.rounds--;
        let sum;
        if (o)
        {
            sum = this.summarize(o,this.values);
            //if(sum>=this.THERESHOLD)
            //    return;
            if (sum>=this.total/2)
                return;
        }
        return this.minimize();
    }
};
