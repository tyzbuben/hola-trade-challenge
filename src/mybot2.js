'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log) {
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.THERESHOLD = 9;
        this.minimal = -1;
        this.min_index;
        this.return_values = this.values.slice();
        this.return_counts = this.counts.slice();
        this.outerArr = [];
        this.alreadyOffered = [];
        for (let i = 0; i < counts.length; i++) {
            this.total += counts[i] * values[i];
        }
        this.offerIndex;
    }
    summarize(arr) {
        let sum = 0;
        for (let i = 0; i < arr.length; i++)
            sum += this.values[i] * arr[i];
        return sum;
    }

    minimize() {
        if (this.minimal < 0) {
            this.minimal = 0;
            this.log(`minimal = -1`)
            for (let i = 0; i < this.return_counts.length; i++) {
                if (!this.values[i])
                    this.return_counts[i] = 0;
            }
            this.min_index = this.next_min();
            return this.return_counts;
        }
        while (true) {
            if (this.return_counts[this.min_index] == 0) {
                this.log(`enternity`)
                this.log(`${this.return_counts}`)
                this.min_index = this.next_min();
            } else {
                this.return_counts[this.min_index]--;
                return this.return_counts;
            }
        }
    }

    next_min() {
        let min;
        let minimal_index;
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0) {
                min = this.return_values[i];
                break;
            }
        }
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0
                && this.return_values[i] <= min
                && this.return_values[i] >= this.minimal) {
                min = this.return_values[i];
                this.minimal = min;
                minimal_index = i;
                this.return_values[i] = 0;
                this.log(`находим минимальный`)
            }
        }
        return minimal_index;
    }



    addToOuter(arr) {
        if (this.outerArr.length == 0) {
            this.outerArr.push(arr);
            return;
        }
        let hasDuplicate = true;
        for (let i = 0; i < this.outerArr.length; i++) {
            let a = this.equal(this.outerArr[i], arr);
            if (a) {
                return;
            }
            hasDuplicate = a;

        }
        if (!hasDuplicate) {
            this.outerArr.push(arr);
        }
    }

    offer(o) {
        if (this.rounds == 5) {
            this.minimi(this.counts, 0);
            this.deleteUneven();
            //this.quickSort(this.outerArr, 0, this.outerArr.length - 1);
            this.bubbleSort(this.outerArr);
            for (let i = 0; i < this.outerArr.length; i++) {
                this.log(`${this.summarize(this.outerArr[i])}`);
            }

            this.log(`COUNTED ONCE`);
        }
        //this.log(`${this.rounds} rounds left - simple_str_bot`);
        this.rounds--;
        if (o) {
            this.log(`ENTERED INSIDE`);
            let sum;
            sum = this.summarize(o, this.values);
            this.log(`THIS IS OFFERINF`);
            this.log(`${o}`);
            this.log(`${sum}`);
            if (this.rounds == 0 && sum != 0) {
                this.log(`LAST CHANCE`);
                return;
            }
            if (sum >= this.THERESHOLD)
                return;
            //if (sum >= this.total / 2) {
            //   this.log(`${sum} SUM IS GOOD ENOUGH`);
            //    return;
            //}
        }
        //o = this.counts.slice();
        //this.outerArr.push(o);
        //this.minimi(o, 0);
        //this.positiveOuter(this.outerArr);
        //this.log(`${this.outerArr} OUTER ARRAY`);
        //this.clearArr(this.outerArr);
        //this.log(JSON.stringify(this.outerArr, null, 4) + `CLEAR OUTER ARRAY`);
        //this.log(`${this.counts} counts`);
        //this.quickSort(this.outerArr, 0, this.outerArr.length - 1);
        //for (let i = 0; i < this.outerArr.length; i++) {
        //    this.log(`VALUER - ${this.summarize(this.outerArr[i])}`);
        //}
        //return this.minimize();
        this.THERESHOLD--;
        this.log(`GOING TO MAKE OFFER`);
        return this.makeOffer();
    }



    makeOffer() {
        this.log(`CAME INSIDE MAKEOFFER`);
        if (!this.offerIndex) {
            this.offerIndex = this.outerArr.length - 1;
            let offer = this.outerArr[this.offerIndex];
            this.offerIndex--;
            this.log(`${offer}`);
            let a = this.optimizeOffer(offer);
            this.alreadyOffered.push(a);
            return a;
        }

        let a = true;
        let offer;
        while (a) {
            let offer = this.outerArr[this.offerIndex];
            this.offerIndex--;
            this.log(`${offer}`);
            let optimized = this.optimizeOffer(offer);
            if(!this.contains(optimized)){
                offer = optimized;
                break;
            }
        }
        return offer;
    }


    contains(arr) {
        let result = false;
        for (let i = 0; i < this.alreadyOffered.length; i++) {
            if (this.alreadyOffered[i][0] == arr[0] &&
                this.alreadyOffered[i][1] == arr[1] &&
                this.alreadyOffered[i][2] == arr[2]) {
                result = true;
                break;
            }
        }
        return result;
    }


    optimizeOffer(offer) {
        for (let i = offer.length; i >= 0; i--) {
            if (this.values[i] == 0) {
                offer[i] = 0;
            }
        }
        return offer;
    }

    minimi(o, index) {
        let arr = o.slice();
        //this.log(`${arr} - arr,${index} - index dive into`);
        if (index == arr.length) {
            return;
        }

        for (let i = arr[index]; i >= 0; i--) {
            arr[index] = i;
            //this.log(`${arr[index]} index value`);
            //this.log(`${arr} inner ARRAY`);
            let additionArr = arr.slice();
            //this.outerArr.push(additionArr);
            this.addToOuter(additionArr);
            //arr[index] = i - 1;
            //this.minimi(arr, index + 1);
            arr[index] = i;
            this.minimi(arr, index + 1);
            this.minimi(arr, index + 2);
        }
    }

    equal(arr1, arr2) {
        let equal = true;
        for (let i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                equal = false;
            }
        }
        return equal;
    }

    swap(arr, leftVal, rightVal) {
        this.log(`${leftVal}`);
        this.log(`${rightVal}`);
        let temp = arr[leftVal];
        arr[leftVal] = arr[rightVal];
        arr[rightVal] = temp;
    }

    partition(arr, left, right) {
        this.log(`partition`);
        let pivot = Math.floor((right + left) / 2);
        let i = left;
        let j = right;
        while (i <= j) {
            while (this.summarize(arr[i]) < this.summarize(arr[pivot])) {
                i++;
            }
            while (this.summarize(arr[j]) > this.summarize(arr[pivot])) {
                j--;
            }
            if (i <= j) {
                this.swap(arr, i, j);
                i++; j--;
            }
        }
        return i;
    }

    quickSort(arr, left, right) {
        let index;
        this.log(`quickSort`);
        if (arr.length > 1) {
            index = this.partition(arr, left, right);

            if (left < index - 1) {
                this.quickSort(arr, left, index - 1);
            }

            if (index < right) {
                this.quickSort(arr, index, right);
            }
        }
        return arr;
    }

    bubbleSort(arr) {
        var count = arr.length - 1;
        for (var i = 0; i < count; i++)
            for (var j = 0; j < count - i; j++)
                if (this.summarize(arr[j]) > this.summarize(arr[j + 1])) {
                    var max = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = max;
                }
        return arr;
    }

    deleteUneven() {
        for (let i = 0; i < this.outerArr.length; i++) {
            if (this.outerArr[i][0] == this.counts[0] &&
                this.outerArr[i][1] == this.counts[1]
                && this.outerArr[i][2] == this.counts[2]) {
                this.outerArr.splice(i, 1);
                break;
            }
        }
    }
};

    /*
anotheRquicksort(array) {
if (array.length <= 1) {
return array;
}
 
let pivot = this.summarize(array[0]);
 
let left = []; 
let right = [];
 
for (let i = 1; i < array.length; i++) {
this.summarize(array[i]) < pivot ? left.push(array[i]) : right.push(array[i]);
}
 
return this.anotheRquicksort(left).concat(pivot, this.anotheRquicksort(right));
};*/

    /*anotheRquicksort(data) {
    if (data.length == 0) return [];
     
    let left = [], right = [], pivot = this.summarize(data[0]);
     
    for (let i = 1; i < data.length; i++) {
    if(this.summarize(data[i]) < pivot)
    left.push(data[i])
    else
    right.push(data[i]);
    }
     
    return this.anotheRquicksort(left).concat(pivot, this.anotheRquicksort(right));
    }*/

/* clearArr(arr){
let a =[];
for(let i =0;i<arr.length;i++){
if(!this.equalInArr(a,arr[i])){
a.push(arr[i]);
}
}
}*/

/*equalInArr(arr,elem){
if(!elem){return false;}
let equal = false;
for(let i = 0;i<arr.length;i++){
equal = this.equal(arr[i],elem);
}
return equal;
}*/

/* equal(arr1,arr2){
let equal = true;
for(let i =0;i<arr1.length;i++){
if(arr1[i]!=arr2[i]){
equal = false;
}
}
return equal;
 
positiveOuter(arr) {
for (let i = 0; i < arr.length; i++) {
this.makeValuesPositive(arr[i]);
}
}
 
makeValuesPositive(arr) {
for (let i = 0; i < arr.length; i++) {
if (arr[i] < 0) {
arr[i] = arr[i] * (-1);
}
}
}
}*/