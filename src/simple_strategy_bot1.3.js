'use strict'; /*jslint node:true*/

module.exports = class Agent {
    constructor(me, counts, values, max_rounds, log) {
        this.counts = counts;
        this.values = values;
        this.rounds = max_rounds;
        this.log = log;
        this.total = 0;
        this.THERESHOLD = 7;
        this.minimal = -1;
        this.min_index;
        this.return_values = this.values.slice();
        this.return_counts = this.counts.slice();
        this.outerArr = [];
        for (let i = 0; i < counts.length; i++)
            this.total += counts[i] * values[i];
    }
    summarize(arr, vals) {
        let sum = 0;
        for (let i = 0; i < arr.length; i++)
            sum += vals[i] * arr[i];
        return sum;
    }

    minimize() {
        if (this.minimal < 0) {
            this.minimal = 0;
            this.log(`minimal = -1`)
            for (let i = 0; i < this.return_counts.length; i++) {
                if (!this.values[i])
                    this.return_counts[i] = 0;
            }
            this.min_index = this.next_min();
            return this.return_counts;
        }
        while (true) {
            if (this.return_counts[this.min_index] == 0) {
                this.log(`enternity`)
                this.log(`${this.return_counts}`)
                this.min_index = this.next_min();
            } else {
                this.return_counts[this.min_index]--;
                return this.return_counts;
            }
        }
    }

    next_min() {
        let min;
        let minimal_index;
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0) {
                min = this.return_values[i];
                break;
            }
        }
        for (let i = 0; i < this.return_values.length; i++) {
            if (this.return_values[i] != 0
                && this.return_values[i] <= min
                && this.return_values[i] >= this.minimal) {
                min = this.return_values[i];
                this.minimal = min;
                minimal_index = i;
                this.return_values[i] = 0;
                this.log(`находим минимальный`)
            }
        }
        return minimal_index;
    }
    //находим перестановки, правда отрицательные^^
    minimi(o, index) {
        let arr = o.slice();
        this.log(`${arr} - arr,${index} - index dive into`);
        if (index == arr.length) {
            this.log(` return`);
            return;
        }

        for (let i = arr[index]; i >= 0; i--) {
            arr[index] = i;
            this.log(`${arr[index]} index value`);
            this.log(`${arr} inner ARRAY`);
            this.outerArr.push(arr);
            arr[index] = i-1;
            this.minimi(arr, index + 1);
        }
    }

    offer(o) {
        this.log(`${this.rounds} rounds left - simple_str_bot`);
        this.rounds--;
        let sum;
        if (o) {
            sum = this.summarize(o, this.values);
            if (this.rounds == 0) {
                return;
            }
            if (sum >= this.THERESHOLD)
                return;
            //if (sum >= this.total / 2)
            // return;
        }
        o = this.counts.slice();
        this.outerArr.push(o);
        this.minimi(o, 0);
        this.log(`${this.outerArr} OUTER ARRAY`);
        //this.clearArr(this.outerArr);
        this.log(JSON.stringify(this.outerArr, null, 4) + `CLEAR OUTER ARRAY`);
        //return this.minimize();
        return;
    }
};

/* clearArr(arr){
let a =[];
for(let i =0;i<arr.length;i++){
if(!this.equalInArr(a,arr[i])){
a.push(arr[i]);
}
}
}*/

/*equalInArr(arr,elem){
if(!elem){return false;}
let equal = false;
for(let i = 0;i<arr.length;i++){
equal = this.equal(arr[i],elem);
}
return equal;
}*/

/* equal(arr1,arr2){
let equal = true;
for(let i =0;i<arr1.length;i++){
if(arr1[i]!=arr2[i]){
equal = false;
}
}
return equal;
}*/